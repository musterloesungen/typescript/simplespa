import Mustache from 'mustache';

export function Component(selector: string, data: any = {}) {
    return (constructor: Function) => {
        const template = (constructor as any).template;
        const rendered = Mustache.render(template, data);

        const elements = document.querySelectorAll(selector);
        elements.forEach(element => {
            element.innerHTML = rendered;
        });
    }
}
