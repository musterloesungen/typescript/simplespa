import { Component } from '../decorators/Component';

@Component('app-root')
export class MyComponent {
    static template = `<app-hello></app-hello>`;
}
