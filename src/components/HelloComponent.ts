import { Component } from '../decorators/Component';

@Component('app-hello', { message: 'Hello, World!' })
export class MyComponent {
    static template = `
    <div>
      <h1>{{message}}</h1>
    </div>
  `;
}
